# import CPK library
from ROOT import gSystem
libs = gSystem.GetDynamicPath() + ":/usr/lib:/usr/local/lib:/opt/lib:/opt/local/lib"
gSystem.SetDynamicPath(libs)
gSystem.Load('libcpk')

from ROOT import Double, TFile, CPK

SHAPER_FILE = "../data/cern-atlas-tilecalorimeter-pulse-shape.dat"
OUTPUT_FILE = "../output/dataset.root"
PULSE_SIZE = 7
DATASET_LENGTH = 1000000

# create the output file
outputFile = TFile(OUTPUT_FILE, "RECREATE")
outputFile.cd()

# setup the generator
generator = CPK.TDatasetGenerator(DATASET_LENGTH, PULSE_SIZE, SHAPER_FILE)
generator.SetOccupancy(.1)
generator.SetOccupancy(.1)
generator.SetLuminosity(40)
generator.SetGaussianNoiseParams(0, 1.0)

# generate a serial dataset
dataset = generator.GetSerialDataset()
dataset.Print()
dataset.Write("serial");

# generate multiple dataset
dataset = generator.GetSegmentedDataset(PULSE_SIZE)
dataset.Print()
dataset.Write("segmented");

outputFile.Close()

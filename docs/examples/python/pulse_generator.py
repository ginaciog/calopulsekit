# import CPK library
from ROOT import gSystem
libs = gSystem.GetDynamicPath() + ":/usr/lib:/usr/local/lib:/opt/lib:/opt/local/lib"
gSystem.SetDynamicPath(libs)
gSystem.Load('libcpk')

# setup the generator
from ROOT import Double, CPK
SHAPER_FILE = "../data/cern-atlas-tilecalorimeter-pulse-shape.dat"
PULSE_SIZE = 7
generator = CPK.TPulseGenerator(PULSE_SIZE, SHAPER_FILE)

# generate a single pulse
amplitude = Double(1.0)
phase = Double(1.0)
pulse = generator.GeneratePulse(amplitude, phase)
print(f"Generated pulse with amplitude={amplitude} phase={phase}")
pulse.Print()

# generate multiple pulses
pulses = generator.GenerateNPulses(1000)
pulses.Print()

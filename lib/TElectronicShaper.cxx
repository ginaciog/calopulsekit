/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "TElectronicShaper.h"

#include <vector>
#include <fstream>
#include <iomanip>
#include <stdexcept>

ClassImp(CPK::TElectronicShaper)

   using namespace ROOT;

namespace CPK {

TElectronicShaper::TElectronicShaper(const Char_t *filepath) : TObject()
{
   fShaperResolution = 0;
   fShaperZeroIndex  = 0;
   this->ReadShaper(filepath);
}

TElectronicShaper::TElectronicShaper(const TElectronicShaper &source) : TObject(source)
{
   fShaper           = TVectorD(source.fShaper);
   fShaperResolution = source.fShaperResolution;
   fShaperZeroIndex  = source.fShaperZeroIndex;
}

TElectronicShaper::~TElectronicShaper() {}

void TElectronicShaper::ReadShaper(const Char_t *filepath)
{
   std::vector<Double_t> timeSeries;
   std::vector<Double_t> weightSeries;
   std::ifstream         file;

   file.open(filepath);
   if (file) {
      Int_t    i(0);
      Double_t a, b;
      while (file >> a >> b) // loop on the input operation, not eof
      {
         timeSeries.push_back(a);
         weightSeries.push_back(b);
         if (a == 0.0) fShaperZeroIndex = i;
         i++;
      }
   } else {
      throw std::invalid_argument("invalid shaper path");
   }

   // read eletronic pulse shaper file
   Int_t nsize = weightSeries.size();

   fShaperResolution = nsize > 2 ? timeSeries[1] - timeSeries[0] : timeSeries[0];

   fShaper.ResizeTo(nsize);
   for (Int_t j = 0; j < nsize; j++) {
      fShaper[j] = weightSeries[j];
   }

   weightSeries.clear();
   timeSeries.clear();
}

} // namespace CPK

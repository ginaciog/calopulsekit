/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "TPulseGenerator.h"

ClassImp(CPK::TPulseGenerator)

   using namespace ROOT;

namespace CPK {

TPulseGenerator::TPulseGenerator(Int_t nsamples, const Char_t *shaperpath)
   : TObject(), fSize(nsamples), fElectronicShaper(shaperpath), fSamplingRate(25), fPhaseMean(0), fPhaseStddev(0),
     fDefMean(0), fDefStddev(0), fPed(0), fLag(0), fDistArg1(1024), fDistArg2(0), fAmpDist(EAmpDist::kUniform),
     fGenerator(0), fHistogram(0)
{
}

TPulseGenerator::TPulseGenerator(const TPulseGenerator &source)
   : TObject(source), fElectronicShaper(source.fElectronicShaper)
{
}

TPulseGenerator::~TPulseGenerator() {}

void TPulseGenerator::SetPhaseParams(Double_t mean, Double_t stddev)
{
   fPhaseMean   = mean;
   fPhaseStddev = stddev;
}

void TPulseGenerator::SetDeformationParams(Double_t mean, Double_t stddev)
{
   fDefMean   = mean;
   fDefStddev = stddev;
}

void TPulseGenerator::SetPedestal(Double_t ped)
{
   fPed = ped;
}

void TPulseGenerator::UseUniformDistribution(Double_t vmax)
{
   fAmpDist  = EAmpDist::kUniform;
   fDistArg1 = vmax;
}

void TPulseGenerator::UseExpDistribution(Double_t mean)
{
   fAmpDist  = EAmpDist::kExp;
   fDistArg1 = mean;
}

void TPulseGenerator::UseGaussianDistribution(Double_t mean, Double_t stddev)
{
   fAmpDist  = EAmpDist::kGauss;
   fDistArg1 = mean;
   fDistArg2 = stddev;
}

void TPulseGenerator::UseHistogramDistribution(const TH1 *hist)
{
   fAmpDist   = EAmpDist::kHistogram;
   fHistogram = hist;
}

Double_t TPulseGenerator::RandomAmplitude()
{
   Double_t amp = 0;

   switch (fAmpDist) {
   case EAmpDist::kUniform: amp = fGenerator->Integer((Int_t)fDistArg1); break;
   case EAmpDist::kExp: amp = fGenerator->Exp(fDistArg1); break;
   case EAmpDist::kGauss: amp = fGenerator->Gaus(fDistArg1, fDistArg2); break;
   case EAmpDist::kHistogram: amp = fHistogram->GetRandom(); break;
   }

   return amp;
}

TVectorD *TPulseGenerator::GeneratePulse(Double_t &amp, Double_t &phase, Double_t lag)
{
   TVectorD *pulse = new TVectorD(fSize);

   // new generator with random seed
   fGenerator = new TRandom(0);

   // random amplitude
   Double_t lAmplitude = this->RandomAmplitude();

   // random phase - normal
   Double_t lPhase = fGenerator->Gaus(fPhaseMean, fPhaseStddev);

   const TVectorD &shaper = *(fElectronicShaper.GetShaper());
   Double_t        shr    = fElectronicShaper.GetShaperResolution();
   Double_t        shzi   = fElectronicShaper.GetShaperZeroIndex();

   for (int i = 0; i < fSize; i++) {
      // random deformation - normal
      Double_t deformation = fGenerator->Gaus(fDefMean, fDefStddev);
      Int_t    shaperIndex =
         Int_t(shzi) - Int_t(lag / shr) + (i - Int_t(fSize) / 2) * (fSamplingRate / shr) + round(lPhase / shr);

      if (shaperIndex < 0 || shaperIndex > shaper.GetNoElements() - 1) shaperIndex = 0;

      pulse->operator[](i) = lAmplitude * shaper[shaperIndex] + fPed + deformation;
   }

   delete fGenerator;

   amp   = lAmplitude;
   phase = lPhase;
   return pulse;
}

TVectorD *TPulseGenerator::GenerateDeterministicPulse(Double_t amp, Double_t phase, Double_t lag)
{
   TVectorD *pulse = new TVectorD(fSize);

   // new generator with random seed
   fGenerator = new TRandom(0);

   Double_t lAmplitude = amp;
   Double_t lPhase     = phase;

   const TVectorD &shaper = *(fElectronicShaper.GetShaper());
   Double_t        shr    = fElectronicShaper.GetShaperResolution();
   Double_t        shzi   = fElectronicShaper.GetShaperZeroIndex();

   for (int i = 0; i < fSize; i++) {
      // random deformation - normal
      Double_t deformation = fGenerator->Gaus(fDefMean, fDefStddev);
      Int_t    shaperIndex =
         Int_t(shzi) - Int_t(lag / shr) + (i - Int_t(fSize) / 2) * (fSamplingRate / shr) + round(lPhase / shr);

      if (shaperIndex < 0 || shaperIndex > shaper.GetNoElements() - 1) shaperIndex = 0;

      pulse->operator[](i) = lAmplitude * shaper[shaperIndex] + fPed + deformation;
   }

   delete fGenerator;

   return pulse;
}

void TPulseGenerator::AddPileup(TVectorD *pulse, Double_t occupancy)
{
   Int_t nsize = pulse->GetNoElements();

   Double_t pileupAmplitude, pileupPhase, lag;
   TVectorD pileup(nsize);
   pileup.Zero();

   TRandom generator(0);

   for (Int_t j = 0; j < nsize; j++) {
      if (generator.Uniform(0.0, 1.0) < occupancy) {
         lag                = (j - Int_t(nsize) / 2) * fSamplingRate;
         TVectorD *tmpPulse = this->GeneratePulse(pileupAmplitude, pileupPhase, lag);
         pileup += (*tmpPulse);
         delete tmpPulse;
      }
   }

   (*pulse) += pileup;
}

void TPulseGenerator::AddGaussianNoise(TVectorD *pulse, Double_t noiseMean, Double_t noiseStddev)
{
   Int_t    nsize = pulse->GetNoElements();
   TVectorD noise(nsize);

   // generate noise
   TRandom generator(0);
   for (Int_t i = 0; i < nsize; i++) noise[i] = fPed + generator.Gaus(noiseMean, noiseStddev);

   (*pulse) += noise;
}

const TTree *TPulseGenerator::GenerateNPulses(ULong_t npulses)
{
   Double_t  amplitude, phase;
   Double_t *pulse    = 0;
   TVectorD *tmpPulse = 0;

   TTree *tree = new TTree("pulses", "Generated pulses");
   tree->Branch("pulse_size", &fSize, "pulse_size/I");
   tree->Branch("amplitude", &amplitude, "amplitude/D");
   tree->Branch("phase", &phase, "phase/D");
   tree->Branch("pulse", pulse, "pulse[pulse_size]/D");

   for (ULong_t i = 0; i < npulses; i++) {
      tmpPulse = this->GeneratePulse(amplitude, phase);
      pulse    = tmpPulse->GetMatrixArray();
      tree->Fill();
   }

   return tree;
}

} // namespace CPK

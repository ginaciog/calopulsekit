/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "TDatasetGenerator.h"

#include "TRandom.h"

ClassImp(CPK::TDatasetGenerator)

   using namespace ROOT;

namespace CPK {

TDatasetGenerator::TDatasetGenerator(ULong64_t length, UInt_t pulseSize, const Char_t *shaperPath)
   : TObject(), fLength(length), fSamples(0), fAmplitudes(0), fOccupancy(0), fLuminosity(0), fNoiseMean(0),
     fNoiseStdDev(0)
{
   fPulseGenerator = new TPulseGenerator(pulseSize, shaperPath);
}

TDatasetGenerator::TDatasetGenerator(const TDatasetGenerator &source)
   : TObject(source), fLength(source.fLength), fPulseGenerator(source.fPulseGenerator), fSamples(source.fSamples),
     fAmplitudes(source.fAmplitudes), fOccupancy(source.fOccupancy), fLuminosity(source.fLuminosity),
     fNoiseMean(source.fNoiseMean), fNoiseStdDev(source.fNoiseStdDev)
{
}

TDatasetGenerator::~TDatasetGenerator() {}

void TDatasetGenerator::SetLuminosity(Double_t luminosity)
{
   fLuminosity = luminosity;
}

void TDatasetGenerator::SetOccupancy(Double_t occupancy)
{
   fOccupancy = occupancy;
}

void TDatasetGenerator::SetGaussianNoiseParams(Double_t mean, Double_t stddev)
{
   fNoiseMean   = mean;
   fNoiseStdDev = stddev;
}

void TDatasetGenerator::GenerateSamples()
{
   // configure the pulse generator
   fPulseGenerator->SetPhaseParams(0, 0);
   fPulseGenerator->SetDeformationParams(0, 0);
   fPulseGenerator->SetPedestal(0);
   fPulseGenerator->UseExpDistribution(fLuminosity);

   TRandom random(0);

   Double_t  noise, amplitude, phase;
   TVectorD *pulse;

   fSamples.ResizeTo(fLength);
   fSamples.Zero();
   fAmplitudes.ResizeTo(fLength);
   fAmplitudes.Zero();

   for (ULong64_t i = 0; i < fLength; i++) {
      noise = random.Gaus(fNoiseMean, fNoiseStdDev);

      // background noise
      fSamples[i] += noise;

      // pileup noise
      // probability to exist a pulse in this sample
      if (random.Uniform(0.0, 1.0) < fOccupancy) {
         pulse          = fPulseGenerator->GeneratePulse(amplitude, phase);
         fAmplitudes[i] = amplitude;
         fSamples[i]    = noise;

         for (Int_t j = 0; j < pulse->GetNoElements(); j++) {
            Int_t offset = j - pulse->GetNoElements() / 2;

            // skip out of bound index
            if (offset < 0 && i < (UInt_t)(-1 * offset)) continue;
            if (i >= fLength - offset) continue;

            fSamples[i + offset] += pulse->operator[](j);
         }
         delete pulse;
      }
   }
}

TTree *TDatasetGenerator::GetSerialDataset()
{
   this->GenerateSamples();

   Double_t sample;
   Double_t amplitude;

   TTree *tree = new TTree("dataset", "Dataset");
   tree->Branch("sample", &sample, "sample/D");
   tree->Branch("amplitude", &amplitude, "amplitude/D");

   for (ULong_t i = 0; i < fLength; i++) {
      sample    = fSamples[i];
      amplitude = fAmplitudes[i];
      tree->Fill();
   }

   return tree;
}

TTree *TDatasetGenerator::GetSegmentedDataset(UInt_t windowSize)
{
   this->GenerateSamples();

   Double_t pulse[windowSize];
   Double_t amplitude[windowSize];

   Char_t pulseBranchName[64];
   Char_t amplitudeBranchName[64];
   sprintf(pulseBranchName, "pulse[%d]/D", windowSize);
   sprintf(amplitudeBranchName, "amplitude[%d]/D", windowSize);

   TTree *tree = new TTree("dataset", "Segmented Dataset");
   tree->Branch("pulse", pulse, pulseBranchName);
   tree->Branch("amplitude", amplitude, amplitudeBranchName);

   UInt_t iterations = fLength - (fLength % windowSize);
   for (ULong_t i = 0; i < iterations; i += windowSize) {
      for (ULong_t j = 0; j < windowSize; j++) {
         pulse[j]     = fSamples[i + j];
         amplitude[j] = fAmplitudes[i + j];
      }
      tree->Fill();
   }

   return tree;
}

} // namespace CPK

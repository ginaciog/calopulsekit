CPK -- Calo Pulse Kit 
=====================

<div align="center">![CPK Logo](docs/logo.png?raw=true "CPK Logo")</div>

A data generation toolkit for particle accelerator calorimetry applications based on ROOT.

This repository can be found online on CERN's [GitLab][CPK Repo].

## Get started

### Requirements
Building CPK requires [CMake](https://cmake.org/) 3.1 or higher, GCC ≥ 6.2 or
higher and [ROOT](https://root.cern.ch/) framework ≥ v6.0.

This library is written according to the C++14 standard. Please make sure these dependencies are installed properly
before building.

### Instalation
Compile and install the shared library:

```bash
mkdir build
cd build
cmake ../
make
sudo make install
```

### Usage
In the folder `docs/examples` the are examples in both ROOT macros (CINT) and Python.

To run these examples, just execute:

```bash
cd docs/examples/cint
root -l -q -b dataset_generator.C
```

or

```bash
cd docs/examples/python
pyhton dataset_generator.py
```

You can see the output in the folder `docs/examples/output`.

## Author

Guilherme Inácio Gonçalves <ginaciog@cern.ch>

## Developer Information

### Documentation

CPK's internal documentation is written using [Doxygen] ≥ 1.8.
Make sure you have Doxygen and Graphviz installed. You can
build it *after* the configuration via `cmake` in your build directory.
Simply call:

```bash
make docs
```


[CPK Repo]: https://gitlab.cern.ch/ginaciog/calopulsekit

/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef TDATASET_GENERATOR_H
#define TDATASET_GENERATOR_H

#include "TPulseGenerator.h"

#include "TObject.h"
#include "TVectorD.h"
#include "TTree.h"

using namespace ROOT;

namespace CPK {

class TDatasetGenerator : public TObject {
private:
   ULong64_t        fLength;         // dataset length
   TPulseGenerator *fPulseGenerator; // pulse generator
   TVectorD         fSamples;        // generated samples
   TVectorD         fAmplitudes;     // generated amplitudes
   Double_t         fOccupancy;      // pileup occupancy [0,1]
   Double_t         fLuminosity;     // luminosity intensity
   Double_t         fNoiseMean;      // gaussian noise mean
   Double_t         fNoiseStdDev;    // gaussian noise std dev

   void GenerateSamples();

public:
   TDatasetGenerator(ULong64_t, UInt_t, const Char_t *);
   TDatasetGenerator(const TDatasetGenerator &);
   virtual ~TDatasetGenerator();

   TTree *GetSerialDataset();
   TTree *GetSegmentedDataset(UInt_t = 7);
   void   SetGaussianNoiseParams(Double_t, Double_t);
   void   SetLuminosity(Double_t);
   void   SetOccupancy(Double_t);

   ClassDef(TDatasetGenerator, 1)
};

} // namespace CPK
#endif /* TDATASET_GENERATOR_H */

/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef TPULSEGENERATOR_H
#define TPULSEGENERATOR_H

#include "TElectronicShaper.h"

#include "TObject.h"
#include "TVectorD.h"
#include "TTree.h"
#include "TRandom.h"
#include "TH1.h"

using namespace ROOT;

namespace CPK {

class TPulseGenerator : public TObject {
private:
   enum EAmpDist { kUniform, kExp, kGauss, kHistogram };

   Int_t             fSize;                // pulse samples length
   TElectronicShaper fElectronicShaper;    // eletronic shaper
   Int_t             fSamplingRate;        // sampling rate
   Double_t          fPhaseMean;           // phase mean
   Double_t          fPhaseStddev;         // phase standard deviation
   Double_t          fDefMean;             // deformation mean
   Double_t          fDefStddev;           // deformation standard deviation
   Double_t          fPed;                 // pedestal
   Double_t          fLag;                 // pulse lag in nanoseconds
   Double_t          fDistArg1, fDistArg2; // amplitude generator agrs
   EAmpDist          fAmpDist;             // amplitude distribution
   TRandom *         fGenerator;           // generator instance
   const TH1 *       fHistogram;           // a histogram instance

   Double_t RandomAmplitude();

public:
   TPulseGenerator(Int_t nsamples, const Char_t *shaperpath);
   TPulseGenerator(const TPulseGenerator &);
   virtual ~TPulseGenerator();

   // getters and setters
   Double_t GetSamplingRate() const { return fSamplingRate; }
   Double_t GetPhaseMean() const { return fPhaseMean; }
   Double_t GetPhaseStddev() const { return fPhaseStddev; }
   void     SetPhaseParams(Double_t mean, Double_t stddev);
   Double_t GetDeformationMean() const { return fDefMean; }
   Double_t GetDeformationStddev() const { return fDefStddev; }
   void     SetDeformationParams(Double_t mean, Double_t stddev);
   Double_t GetPedestal() const { return fPed; }
   void     SetPedestal(Double_t ped);
   Int_t    GetPulseSize() const { return fSize; }

   // generator
   void         UseUniformDistribution(Double_t vmax);
   void         UseExpDistribution(Double_t mean);
   void         UseGaussianDistribution(Double_t mean, Double_t stddev);
   void         UseHistogramDistribution(const TH1 *hist);
   void         AddPileup(TVectorD *pulse, Double_t occupancy = 0.2);
   void         AddGaussianNoise(TVectorD *pulse, Double_t noiseMean = 0.0, Double_t noiseStddev = 1.0);
   TVectorD *   GeneratePulse(Double_t &amp, Double_t &phase, Double_t lag = 0.0);
   TVectorD *   GenerateDeterministicPulse(Double_t amp = 1.0, Double_t phase = 0.0, Double_t lag = 0.0);
   const TTree *GenerateNPulses(ULong_t npulses);

   ClassDef(TPulseGenerator, 1)
};

} // namespace CPK
#endif /* TPULSEGENERATOR_H */

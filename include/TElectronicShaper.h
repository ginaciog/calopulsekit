/******************************************************************************
 *                         CALO PULSE KIT
 *
 * Bernardo S. Peralva    <bernardo@iprj.uerj.br>
 * Guilherme I. Gonçalves <ggoncalves@iprj.uerj.br>
 *
 * Copyright (C) 2019 Bernardo & Guilherme

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef TELECTRONICSHAPER_H
#define TELECTRONICSHAPER_H

#include "TObject.h"
#include "TVectorD.h"

using namespace ROOT;

namespace CPK {

class TElectronicShaper : public TObject {
private:
   TVectorD fShaper;           // eletronic shaper
   Double_t fShaperResolution; // shaper resolution in nanoseconds
   Int_t    fShaperZeroIndex;  // shaper index of time series zero

   void ReadShaper(const Char_t *filepath);

public:
   TElectronicShaper(const Char_t *filepath);
   TElectronicShaper(const TElectronicShaper &);
   virtual ~TElectronicShaper();

   const TVectorD *GetShaper() const { return &fShaper; }
   Double_t        GetShaperResolution() const { return fShaperResolution; }
   Int_t           GetShaperZeroIndex() const { return fShaperZeroIndex; }

   ClassDef(TElectronicShaper, 1)
};

} // namespace CPK
#endif /* TELECTRONICSHAPER */
